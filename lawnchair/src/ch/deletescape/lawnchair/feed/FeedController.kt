/*
 *     Copyright (c) 2017-2019 the Lawnchair team
 *     Copyright (c)  2019 oldosfan (would)
 *     This file is part of Lawnchair Launcher.
 *
 *     Lawnchair Launcher is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     Lawnchair Launcher is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with Lawnchair Launcher.  If not, see <https://www.gnu.org/licenses/>.
 */

package ch.deletescape.lawnchair.feed

import android.content.Context
import ch.deletescape.lawnchair.LawnchairApp
import ch.deletescape.lawnchair.lawnchairPrefs
import com.android.launcher3.R

private var theController: FeedController? = null

fun getFeedController(c: Context): FeedController {
    if (theController == null) {
        theController = FeedController(c)
    }
    return theController!!;
}

class FeedController(val context: Context) {

    fun getProviders(): List<FeedProvider> {
        val providers = ArrayList<FeedProvider>()
        (context.applicationContext as LawnchairApp).lawnchairPrefs.feedProviders.toList()
                .iterator().forEach {
                    providers.add(ProviderConstructor.inflateFeedProvider(it, context))
                }
        return providers
    }

    companion object {
        fun getDisplayName(provider: String, context: Context): String {
            return when (provider) {
                CalendarEventProvider::class.java.name -> context.getString(
                    R.string.title_feed_provider_calendar)
                FeedWeatherProvider::class.java.name -> context.getString(
                    R.string.title_feed_provider_weather)
                FeedForecastProvider::class.java.name -> context.getString(
                    R.string.title_feed_provider_forecast)
                WikipediaNewsProvider::class.java.name -> context.getString(
                    R.string.title_feed_provider_wikipedia_news)
                else -> error("No such provider ${provider}")
            }
        }

        fun getFeedProviders(): List<String> {
            return listOf(CalendarEventProvider::class.java.name,
                          FeedWeatherProvider::class.java.name,
                          FeedForecastProvider::class.java.name,
                          WikipediaNewsProvider::class.java.name)
        }
    }
}